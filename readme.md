A simple, command line, DDS downscaler. Made by request.

First argument : the path to process, surrounded with quotes

Second argument : the pattern. The last letters of the file. For example, "_n.dds" will process all files ending with "n_.dds"

Third argument : either -d or -s. 
-s will attempt to reduce the texture size to the entered size, and will not process it if it is superior. 
-d will divide the texture size by the indicated factor.

Fourth argument : the size
With -s, use something like 512
With -d, use a divisor, like 2
