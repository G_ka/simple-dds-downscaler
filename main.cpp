#include <QCoreApplication>
#include <QDirIterator>
#include <QTextStream>
#include <QProcess>
#include <QDebug>

void write(const QString& text)
{
    QTextStream ts(stdout);
    ts << text;
}

void printHelp()
{
    write("Wrong usage. Required arguments:\n    \"folder path\" (surrounded by quotes) \n    Pattern\n");
    write("Possible arguments:\n -s New size OR\n -d divisor\n\n");
    write("Example 1: to resize all textures ending with \"_n.dds\" to 512 in the folder \"E:\\Textures\", use \n "
          "Textures_Downscaler.exe \"E:\\Textures\" \"_n.dds\" -s 512 \n\n");
    write("Example 2: to resize all textures ending with \".dds\" to one quarter of their size in the folder \"E:\\Textures\\With Spaces\", use \n"
          "Textures_Downscaler.exe \"E:\\Textures\\With Spaces\" \".dds\" -d 4");
}



bool resizeTexture(const QString& filePath, const QString& mode, const int& newSize)
{
    QProcess texDiag;
    texDiag.start("resources/texdiag.exe", QStringList {"info", filePath});
    texDiag.waitForFinished(-1);

    QString texDiagOutput = texDiag.readAllStandardOutput();

    const int currentWidth = texDiagOutput.mid(texDiagOutput.indexOf("width = ")+8, 4).toInt();
    const int currentHeight = texDiagOutput.mid(texDiagOutput.indexOf("height = ")+9, 4).toInt();

    if(currentWidth != currentHeight && mode == "-s")
    {
        write(QCoreApplication::tr("Cannot convert this texture: its height and its width are different. Please use the divisor mode"));
        return false;
    }

    int newWidth = 0;
    int newHeight = 0;


    if(mode == "-s")
    {
        newWidth = newSize;
        newHeight = newSize;
    }
    else if (mode == "-d")
    {
        newWidth = currentWidth / newSize;
        newHeight = currentHeight / newSize;
    }


    if(newWidth > currentWidth && newHeight > currentHeight)
    {
        write(QCoreApplication::tr("The entered size is larger than the current texture size"));
        return false;
    }

    if(newWidth > currentWidth)
        newWidth = currentWidth;

    if(newHeight > currentHeight)
        newHeight = currentHeight;

    QProcess texconv;
    QStringList texconvArg{ "-nologo", "-y", "-w", QString::number(newWidth), "-h", QString::number(newHeight), "-m", "1", filePath};
    texconv.start("resources/texconv.exe", texconvArg);
    texconv.waitForFinished(-1);

    QString texconvOutput = texconv.readAllStandardOutput();

    QStringList texconvArg2 { "-nologo", "-y", "-w", QString::number(newWidth), "-h", QString::number(newHeight), filePath};
    texconv.start("resources/texconv.exe", texconvArg2);
    texconv.waitForFinished();

    texconvOutput += texconv.readAllStandardOutput();

    if(!texconvOutput.contains("error", Qt::CaseInsensitive) && !texconvOutput.contains("failed", Qt::CaseInsensitive))
        return true;

    return false;
}



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    if(argc != 5)
    {
        printHelp();
        return 1;
    }

    QDir path (QCoreApplication::arguments().at(1));
    if(!path.exists())
    {
        write(QCoreApplication::tr("Error. This path does not exist. Aborting."));
        return 1;
    }

    const QString pattern = QCoreApplication::arguments().at(2);
    if(!pattern.contains(".dds", Qt::CaseInsensitive))
    {
        write(QCoreApplication::tr("Error. This pattern does not contain \".dds\". Aborting."));
        return 1;
    }

    const QString mode = QCoreApplication::arguments().at(3);
    if(mode != "-s" && mode != "-d")
    {
        write(QCoreApplication::tr("Error. This mode does not exist. Aborting."));
        return 1;
    }

    const int size = QCoreApplication::arguments().at(4).toInt();
    if(size % 2 != 0)
    {
        write(QCoreApplication::tr("Error. The entered size is not a power of two"));
        return 1;
    }

    QDirIterator it (path, QDirIterator::Subdirectories);

    while (it.hasNext())
    {
        it.next();

        if(it.fileName().endsWith(pattern, Qt::CaseInsensitive))
        {
            write(QCoreApplication::tr("Texture found: ") + it.filePath() + "\n");
            bool success = resizeTexture(it.filePath(), mode, size);

            if(success)
                write(QCoreApplication::tr("Texture succesfully resized!\n"));
            else
                write(QCoreApplication::tr("An error occurred while resizing the texture\n"));
        }
    }

    write(QCoreApplication::tr("Operation completed!"));

    return a.exec();
}



